package repos

var (
	Articles ArticleRepo
	Users    UserRepo
	Topics   TopicRepo
)

func init() {
	Articles = NewArticleRepo("articles")
	Users = NewUserRepo("users")
	Topics = NewTopicRepo("topics")
}
