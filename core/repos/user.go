package repos

import (
	r "github.com/dancannon/gorethink"

	"bitbucket.org/smugisha/baharia-server/core/infra"
	"bitbucket.org/smugisha/baharia-server/core/model"
)

type UserRepo struct {
	Table string
}

func NewUserRepo(t string) UserRepo {
	return UserRepo{
		Table: t,
	}
}

func (repo *UserRepo) FindById(id string) (*model.User, error) {
	var user = new(model.User)
	row, err := r.Table(repo.Table).Get(id).Run(infra.RethinkDB())

	if err != nil {
		return user, err
	}

	if row.IsNil() {
		return nil, nil
	}

	err = row.One(&user)
	return user, err
}

func (repo *UserRepo) FindAll() ([]*model.User, error) {
	var users []*model.User
	rows, err := r.Table(repo.Table).Run(infra.RethinkDB())

	if err != nil {
		return users, err
	}

	err = rows.All(&users)
	if err != nil {
		return users, err
	}

	return users, err
}

func (repo *UserRepo) FindByDigitsId(id string) (*model.User, error) {
	var user = new(model.User)
	query := r.Table(repo.Table).GetAllByIndex("DigitsId", id)

	row, err := query.Run(infra.RethinkDB())
	if err != nil {
		return user, err
	}

	if row.IsNil() {
		return nil, nil
	}

	err = row.One(user)

	return user, err
}

func (repo *UserRepo) FindByPhone(phone string) (*model.User, error) {
	var user = new(model.User)
	query := r.Table(repo.Table).GetAllByIndex("Phone", phone)

	row, err := query.Run(infra.RethinkDB())
	if err != nil {
		return user, err
	}

	if row.IsNil() {
		return nil, nil
	}

	err = row.One(user)

	return user, err
}

func (repo *UserRepo) Store(user *model.User) error {
	response, err := r.Table(repo.Table).Insert(user).RunWrite(infra.RethinkDB())

	if err != nil {
		return err
	}

	// Find new ID of product if needed
	if user.Id == "" && len(response.GeneratedKeys) == 1 {
		user.Id = response.GeneratedKeys[0]
	}

	return nil
}

func (repo *UserRepo) Update(user *model.User) error {
	return nil
}

func (repo *UserRepo) UpdateFields(id string, fields map[string]interface{}) error {
	return nil
}

func (repo *UserRepo) Delete(id string) error {
	return r.Table(repo.Table).Get(id).Delete().Exec(infra.RethinkDB())
}

func (repo *UserRepo) DeleteAll() error {
	return r.Table(repo.Table).Delete().Exec(infra.RethinkDB())
}
