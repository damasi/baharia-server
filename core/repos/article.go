package repos

import (
	r "github.com/dancannon/gorethink"

	"bitbucket.org/smugisha/baharia-server/core/infra"
	"bitbucket.org/smugisha/baharia-server/core/model"
)

type ArticleRepo struct {
	Table string
}

func NewArticleRepo(t string) ArticleRepo {
	return ArticleRepo{
		Table: t,
	}
}

func (repo *ArticleRepo) FindById(id string) (*model.Article, error) {
	var article = new(model.Article)
	row, err := r.Table(repo.Table).Get(id).Run(infra.RethinkDB())
	if err != nil {
		return article, err
	}

	if row.IsNil() {
		return nil, nil
	}

	err = row.One(article)

	return article, err
}

func (repo *ArticleRepo) FindAll() ([]model.Article, error) {
	var articles []model.Article
	rows, err := r.Table(repo.Table).Run(infra.RethinkDB())
	if err != nil {
		return articles, err
	}

	err = rows.All(&articles)
	if err != nil {
		return articles, err
	}

	return articles, err
}

func (repo *ArticleRepo) Store(article *model.Article) error {
	res, err := r.Table(repo.Table).Insert(article).RunWrite(infra.RethinkDB())

	if err != nil {
		return err
	}

	// Find new ID of product if needed
	if article.Id == "" && len(res.GeneratedKeys) == 1 {
		article.Id = res.GeneratedKeys[0]
	}

	return nil
}

func (repo *ArticleRepo) Update(article *model.Article) error {
	res, err := r.Table(repo.Table).Insert(article).RunWrite(infra.RethinkDB())

	if err != nil {
		return err
	}

	// Find new ID of product if needed
	if article.Id == "" && len(res.GeneratedKeys) == 1 {
		article.Id = res.GeneratedKeys[0]
	}

	return nil
}

func (repo *ArticleRepo) FindNewByPage(page int, count int) ([]model.Article, error) {
	var articles []model.Article
	query := r.Table(repo.Table).OrderBy(r.OrderByOpts{
		Index: r.Desc("Published"),
	})
	query = query.Skip((page - 1) * count).Limit(count)
	rows, err := query.Run(infra.RethinkDB())
	if err != nil {
		return articles, err
	}

	err = rows.All(&articles)
	if err != nil {
		return articles, err
	}

	return articles, err
}

func (repo *ArticleRepo) UpdateFields(id string, fields map[string]interface{}) error {
	_, err := r.Table(repo.Table).Get(id).Update(fields).RunWrite(infra.RethinkDB())

	if err != nil {
		return err
	}

	return nil
}

func (repo *ArticleRepo) Delete(id string) error {
	return r.Table(repo.Table).Get(id).Delete().Exec(infra.RethinkDB())
}

func (repo *ArticleRepo) DeleteAll() error {
	return r.Table(repo.Table).Delete().Exec(infra.RethinkDB())
}
