package main

import (
	"flag"
	"fmt"
	"log"

	r "github.com/dancannon/gorethink"

	"bitbucket.org/smugisha/baharia-server/app"
	"bitbucket.org/smugisha/baharia-server/core/config"
	"bitbucket.org/smugisha/baharia-server/core/data"
	"bitbucket.org/smugisha/baharia-server/core/infra"
)

var (
	session *r.Session
)

func main() {
	configPath := flag.String("conf", "config.yaml", "Config file.")
	env := flag.String("env", "", "Environment, overrides config.")
	exampleData := flag.Bool("example-data", false, "Example data.")
	setupDb := flag.Bool("setup-db", false, "Setup database.")

	flag.Parse()

	// Load config
	conf := config.NewConfig()
	err := config.LoadFile(conf, *configPath)

	if err != nil {
		log.Fatal("Error reading config: ", err)
	}

	if *env != "" {
		conf.Env = *env
	}

	// package initalizations
	infra.InitRethinkDB(conf.RethinkDB)

	if *setupDb {
		data.Setup(conf, *exampleData)
	}

	app.Server(conf)

	fmt.Println(conf.Name)
}
