package controllers

import (
	"net/http"
	"log"
	"fmt"
	"encoding/json"

	"bitbucket.org/smugisha/baharia-server/core/model"
	"bitbucket.org/smugisha/baharia-server/core/repos"
	"bitbucket.org/smugisha/baharia-server/app/lib"
)

func TwitterRegistration(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	log.Printf("%#v", r)
	digitsId := r.Form.Get("id")
	phone := r.Form.Get("phone")

	var (
		user *model.User = nil
		err error
	)

	if lib.TwitterOauthEcho(r) {
		user, err = repos.Users.FindByDigitsId(digitsId)
		if err != nil {
			log.Printf("Error: ", err)
		}

		if user == nil {
			user = model.NewUser(digitsId, phone)
			err = repos.Users.Store(user)
			if err != nil {
				log.Printf("Error: ", err)
			}
		}
	}

	if user != nil {
		res, err := json.Marshal(user)
		if err != nil {
			log.Printf("Error %v", err)
		}

		fmt.Fprintf(w, "%v", res)
	} else {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, "not authorised")
	}
}
